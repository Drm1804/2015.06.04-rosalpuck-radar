//
//Класс Дата
//


function ClassDate(){
    var _this = this;
    var dayWeekArr = ['ПН','ВТ','СР','ЧТ','ПТ','СБ','ВС'];
    var dayMonthArr = [31,28,31,30,31,30,31,31,30,31,30,31];
    var today = new Date();
    var dataNews;

    this.returnNowDate = function(){

        var month = today.getMonth() + 1;
        if(month < 10){
            month = '0' + month
        }
        var nowDate = today.getDate() + '.' + month + '.' + today.getFullYear();

        $('#date').html(nowDate);

    };

    this.returnNowTime = function(){
        var cl = today.getSeconds()%2;
        var min = today.getMinutes();
        if(min < 10){
            min = '0' + min;
        }
        var timeType1 = today.getHours()+':'+min;
        var timeType2 = today.getHours()+' '+min;
        if(cl){
            $('#time').html(timeType1);
        } else {
            $('#time').html(timeType2);
        }
    };

    this.setMonthCalendar = function(month, year){
        var mData = new Date(year, month);
        var _Year = mData.getFullYear();

        //Проверяем год а високосность
        if((_Year % 100 != 0)&&(_Year % 4 == 0)){
            dayMonthArr[1] = 29;
        }

        //Проверяем данный, на совпадение с текущими


        $('#b-calendar').html(' '); //Отчищаем календарь

        //Устанавливаем Дни недели

        for(i=0; i < dayWeekArr.length; i++){
            $('.b-calendar').append('<div class="b-calendar__cell b-week-day">'+dayWeekArr[i]+'</div>')

        }

        //Заполняем календарь датами

        var _Month = mData.getMonth();
        var firstDayMonth = mData.getDay();
        if(firstDayMonth == 0){
            firstDayMonth = 7;
        }
        firstDayMonth -= 1 ;

        for (i=0; i < firstDayMonth; i++){
            $('.b-calendar').append('<div class="b-calendar__cell b-calendar__day">&#160;</div>')
        }
        for(j=0; j < dayMonthArr[_Month]; j++){
            var datDate = (j + 1)*1;
            $('.b-calendar').append('<div class="b-calendar__cell b-calendar__day">'+ datDate +'</div>')


        }
        if((today.getFullYear() == _Year) && (today.getMonth() == _Month)){
            var todayDay = today.getDate();
            $('.b-calendar__day:contains('+todayDay+'):first').addClass('b-calendar__day--today');
        }

        _this.addCalendarNews();

    };

    this.todaySetMonthCalendar = function(){

        var todayYear = today.getFullYear();
        var todayMonth = today.getMonth();
        var monthSelect = $('#month');
        var yearSelect = $('#year');
        monthSelect.find('option[value='+todayMonth+']').attr('selected', 'selected');
        yearSelect.find('option[value='+todayYear+']').attr('selected', 'selected');
        var month = monthSelect.find('option:selected').val();
        var year = yearSelect.find('option:selected').val();

        _this.setMonthCalendar(month, year);
    }

    this.addCalendarNews = function(){
        var str = {
            "month": $('#month').find('option:selected').val(),
            "year": $('#year').find('option:selected').val()
        };
        //str.serialize();

        $.ajax({
            type: 'POST',
            url: '../calendar.php',
            data: str,


            success: function(data){
                dataNews = $.parseJSON(data);
                $('#b-calendar-news').html(' ');
                $('.b-calendar__day').removeClass('b-calendar__day--have-news');
                setInterval(function(){
                    $('#b-calendar-news').html(' ');
                    for(i=0; i < dataNews.length; i++){
                        $('#b-calendar-news').append('<div class="b-calendar-news-item b-calendar-news-item--'
                        +dataNews[i].year+'-'+dataNews[i].month_num+'-'+dataNews[i].day+'">' +
                        '<a href="'+dataNews[i].url+'" class="b-one-news b-one-news--calendar">' +
                        '<span class="b-one-news-index__col b-one-news-index__col--all-news">' +
                        '<img class="b-img-all-news" src="'+dataNews[i].img+'" alt=""/>' +
                        '</span><span class="b-one-news-index__col b-one-news-index__col--big-col-all-news">' +
                        '<span class="b-index-news-head">'+dataNews[i].name_news+'</span>' +
                        '<span class="b-index-news-date">'+dataNews[i].day+'' +
                        ' '+ dataNews[i].month_rus +', '+dataNews[i].year+'</span>' +
                        '<span class="b-index-news-description">'+dataNews[i].text_news+
                        '</span></span></a></div>');
                        $('.b-calendar__day:contains('+dataNews[i].day+'):first')
                            .addClass('b-calendar__day--have-news');

                    }
                }, 3000)

            },
            complete: function(){
                $('#b-calendar-news').html('<div class="b-load"></div>');
            }
        });


    };




}

//
//Класс Карусели на главной
//

function Doc(){

    var _this = this;

    var elementsRotate = $('.b-can__thesis');
    var parentRotate = $('.b-can');

    this.typeDisplay = function(){
        if($(document).width() < 960){
            return 'mobile';
        }
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            return 'mobile';
        } else {
            return 'desctop';
        }
    };

    this.rotate = function(countUser){
        var count = Number(parentRotate.attr('data-trans'));
        count = count + countUser;
        elementsRotate.each(function(i){
            var tran = i*72+count;

            elementsRotate.eq(i).css('transform', 'rotateY('+tran+'deg)');

            var divideTran = tran%360;
            //divideTran > 340
            if(divideTran < 50 || divideTran > 300){
                elementsRotate.eq(i).find('.b-can-thesis').removeClass('b-can-thesis--blur');
            } else {
                elementsRotate.eq(i).find('.b-can-thesis').addClass('b-can-thesis--blur');
            }


        });
        parentRotate.attr('data-trans', ''+count+'');

    };

    this.calcDesctopMenu = function(){
        $('.b-head-nav__item').each(function(){
            var width = $(this).find('a').width();
            $(this).width(width + 1) ;
        });
    };

    this.calcSubMenu = function(){

        $('.b-head-sub-nav').each(function(){
            var width = 0;
            $(this).find('.b-head-sub-nav__item').each(function(){
                width += $(this).width()+20;
            });
            $(this).width(width);
        });
    }

    this.mobileMenu = function(){
        if($('.b-head-nav').height() > 0){
            //$('.b-head-nav').css('border', '0px solid #000');
            $('.b-head-nav').animate({maxHeight: '0px'}, 300).css('border', '0px solid #000');

        } else {
            //$('.b-head-nav').css('border', '1px solid #000');
            $('.b-head-nav').animate({maxHeight: '200px'}, 300).css('border', '1px solid #000');

        }

    };

    this.changeAboutItem = function(clickEl){
        var headText = clickEl.html();
        var descriptTetx = clickEl.attr('data-info');
        $('.b-about-show-item h2').html(headText);
        $('.b-about-show-item p').html(descriptTetx);
    };

    this.historySlide = function(){

        //Добавляем нужные стили

        $('.b-history-date__control').show();
        $('.b-history-date-box').width('870')
        //Считаем длину трека
        var trackWidth = 0;

        $('.b-history-item').each(function(){
            trackWidth += $(this).width();
            console.log(trackWidth);

        });
        $('.b-history-date__track-item').width(trackWidth);

        //Собираем события по клику
        $('.b-history-date__control--left').click(function(){
            _this.shiftingHistorySlide('back');
        });
        $('.b-history-date__control--right').click(function(){
            _this.shiftingHistorySlide('next');
        });
    };

    this.shiftingHistorySlide = function(direction){
        var track = $('.b-history-date__track-item');
        var item = $('.b-history-item');
        var shift = track.attr('data-shift');

        if(direction == 'next'){
            shift++;
            shift > item.length - 3 ? shift = item.length - 3 : shift;
        } else {
            shift--;
            shift < 0 ? shift = 0 : shift;
        }
        if(shift == 0){
            $('.b-history-date__control--left').addClass('b-history-date__control--nhover');

        } else if(shift == item.length - 3){
            $('.b-history-date__control--right').addClass('b-history-date__control--nhover');
        } else{
            $('.b-history-date__control--left').removeClass('b-history-date__control--nhover');
            $('.b-history-date__control--right').removeClass('b-history-date__control--nhover');
        }
        track.attr('data-shift', shift);
        track.css('marginLeft', ''+shift* item.width()* (-1)+'');
    }





}